var canvasWidth = 877; //声明画布宽度
var canvasHeight = 672; //声明画布高度
var canvas = document.getElementById('canvas');
canvas.width = canvasWidth;
canvas.height = canvasHeight;
var context = canvas.getContext('2d'); //准备画笔
var image = new Image(); //创建图片对象
var radius = 50; //声明半径
image.src = 'images/pic.png' //获取图片路径
image.onload = function() {
    initCanvas(); //调用初始化画布方法
}

function initCanvas() {
    //擦除整个画布状态
    context.clearRect(0, 0, canvas.width, canvas.height);
    //保存画布的绘制状态
    context.save();
    //开始路径
    context.beginPath();
    //绘制圆形
    var x = Math.random() * (canvas.width - 2 * radius) + radius;
    var y = Math.random() * (canvas.height - 2 * radius) + radius;
    context.arc(x, y, radius, 0, 2 * Math.PI, false);
    //用clip()方法剪切圆形区域
    context.clip();
    context.drawImage(image, 0, 0);
    //恢复canvas保存之前的状态
    context.restore();
}

function reset() {
    initCanvas();
}

function show() {
    radius = 4 * Math.max(canvas.width, canvas.height);
    initCanvas();
}